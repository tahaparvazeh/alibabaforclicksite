package ir.alibabae;

import android.os.Bundle;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import ir.alibabae.Model.NavModel;
import ir.alibabae.Adapter.MyViewPagerAdapter;
import ir.alibabae.Adapter.NavAdapter;
import ir.alibabae.Fragment.BusFragment;
import ir.alibabae.Fragment.HotelFragment;
import ir.alibabae.Fragment.InsideFlightFragment;
import ir.alibabae.Fragment.OutsideFlightFragment;
import ir.alibabae.Fragment.TrainFragment;

public class StartActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;
    ImageView img_menu;
    TabLayout tabLayout;
    ViewPager viewPager;
    MyViewPagerAdapter myViewPagerAdapter;
    TextView txtEmail;
    RecyclerView recyclerViewMenu;
    List<NavModel>models;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        setUpViews();
        setNavItem();
    }

    private void setUpViews() {

        tabLayout = findViewById(R.id.tab_content_tab);
        viewPager = findViewById(R.id.vp_content_viewpager);
        tabLayout.setupWithViewPager(viewPager);
        myViewPagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager());

        myViewPagerAdapter.addFragment(new HotelFragment(),"هتل");
        myViewPagerAdapter.addFragment(new TrainFragment(),"قطار");
        myViewPagerAdapter.addFragment(new BusFragment(),"اتوبوس");
        myViewPagerAdapter.addFragment(new OutsideFlightFragment(),"پرواز خارجی");
        myViewPagerAdapter.addFragment(new  InsideFlightFragment(),"پرواز داخلی");

        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.setCurrentItem(4);

        recyclerViewMenu = findViewById(R.id.rv_nav_menu);
        recyclerViewMenu.setLayoutManager(new LinearLayoutManager(this));


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        img_menu = findViewById(R.id.img_appbar_menu);
        img_menu.setOnClickListener(v -> drawer.openDrawer(Gravity.RIGHT));
    }

    private void setNavItem() {
        models = new ArrayList<>();
        NavModel model = new NavModel();
        model.setDrawable(R.drawable.ic_profile_24);
        model.setTitle("پروفایل کاربری");

        NavModel model2 = new NavModel();
        model2.setDrawable(R.drawable.ic_passengers_24);
        model2.setTitle("لیست مسافران");

        NavModel model3 = new NavModel();
        model3.setDrawable(R.drawable.ic_money_24);
        model3.setTitle("سوابق تراکنش");

        NavModel model4 = new NavModel();
        model4.setDrawable(R.drawable.ic_baseline_shopping_cart_24);
        model4.setTitle("خرید های من");

        NavModel model5 = new NavModel();
        model5.setDrawable(R.drawable.ic_logout_24);
        model5.setTitle("خروج از حساب کاربری");

        models.add(model);
        models.add(model2);
        models.add(model3);
        models.add(model4);
        models.add(model5);

        recyclerViewMenu.setAdapter(new NavAdapter(this,models));

    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(Gravity.RIGHT)){
            drawer.closeDrawer(Gravity.RIGHT);
        }else {
            super.onBackPressed();
        }
    }

}
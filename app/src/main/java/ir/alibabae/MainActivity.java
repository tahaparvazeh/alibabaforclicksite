package ir.alibabae;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

import ir.alibabae.Receiver.NetReceiver;

public class MainActivity extends AppCompatActivity {
    NetReceiver netReceiver;
    ConstraintLayout constraintError;
    Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpViews();
        anim();
        callBack();
    }

    private void callBack() {
        netReceiver.setOnCheckconnection(new NetReceiver.OnCheckconnection() {
            @Override
            public void onErrorReceiver() {
                constraintError.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReceiver() {
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                            Intent intent = new Intent(MainActivity.this,StartActivity.class);
                            startActivity(intent);
                            timer.purge();
                            timer.cancel();
                            finish();
                    }
                },2500,1);
            }
        });
    }

    private void anim() {
//        YoYo.with(Techniques.FadeInUp)
//                .duration(2000)
//                .repeat(10)
//                .playOn(findViewById(R.id.img_main_logo));
    }

    private void setUpViews() {
        timer = new Timer();
        constraintError = findViewById(R.id.cl_main_net_error);
        netReceiver = new NetReceiver();
        registerReceiver(netReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(netReceiver);
    }
}
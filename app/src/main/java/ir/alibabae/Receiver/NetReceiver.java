package ir.alibabae.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetReceiver extends BroadcastReceiver {
    private OnCheckconnection OnCheckconnection;
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo !=null && networkInfo.isConnected()){
            OnCheckconnection.onReceiver();
        }else {
            OnCheckconnection.onErrorReceiver();
        }
    }

    public void setOnCheckconnection(OnCheckconnection onCheckconnection) {
        this.OnCheckconnection = onCheckconnection;
    }

    public interface OnCheckconnection {
        void onErrorReceiver();
        void onReceiver();
    }
}
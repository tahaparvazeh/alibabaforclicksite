package ir.alibabae;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ir.alibabae.Adapter.CitiesAdapter;


public class CitiesActivity extends AppCompatActivity {
    private static final String  URL = "http://alibabatest.epizy.com/citys.json";
    EditText editText;
    RecyclerView recyclerView;
    List<String>citys;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities);
        setUpViews();
        getCity();
    }

    private void getCity() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,URL,null,response -> {

            for (int i = 0; i <response.length() ; i++) {
                try {
                    JSONObject jsonObject = response.getJSONObject(i);
                    String city = jsonObject.getString("city");
                    citys.add(city);
                    Log.i("LOG", "getCity: get");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },error -> {
            Log.i("LOG", "getCity :  "+error);
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    private void setUpViews() {
        citys = new ArrayList<>();
        editText = findViewById(R.id.edt_citiec_seahs);
        recyclerView = findViewById(R.id.rv_cities_city);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CitiesAdapter(this,citys));
    }
}
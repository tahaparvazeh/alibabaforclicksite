package ir.alibabae.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.alibabae.R;


public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.cityViewHolder> {
    Context context;
    List<String> cities;
    public CitiesAdapter(Context context, List<String> models){
        this.cities = models;
        this.context = context;
    }
    @NonNull
    @Override
    public cityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.city_item,parent,false);
        return new cityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull cityViewHolder holder, int position) {
        String city = cities.get(position);
        holder.txtCity.setText(city);
        holder.parent.setOnClickListener(v -> {

        });
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public class cityViewHolder extends RecyclerView.ViewHolder {
        CoordinatorLayout parent;
        TextView txtCity;
        public cityViewHolder(@NonNull View itemView) {
            super(itemView);
            parent =itemView.findViewById(R.id.cl_cityitem_par);
            txtCity=itemView.findViewById(R.id.txt_cityitem_city);
        }
    }
}
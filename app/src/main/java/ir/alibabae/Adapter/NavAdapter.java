package ir.alibabae.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.alibabae.Model.NavModel;
import ir.alibabae.R;

public class NavAdapter extends RecyclerView.Adapter<NavAdapter.NavViewHolder> {
    private Context context;
    private List<NavModel> models;
    public NavAdapter(Context context , List<NavModel> models) {
        this.models = models;
        this.context = context;
    }
    @NonNull
    @Override
    public NavViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_nav_menu,parent,false);
        return new NavViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NavViewHolder holder, int position) {
        NavModel model = models.get(position);
        holder.textTitle.setText(model.getTitle());
        holder.imageIcon.setImageResource(model.getDrawable());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class NavViewHolder extends RecyclerView.ViewHolder{
        TextView textTitle;
        ImageView imageIcon;
        public NavViewHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.txt_navitem_title);
            imageIcon = itemView.findViewById(R.id.img_navitem_icon);
        }
    }
}

package ir.alibabae.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;


import ir.alibabae.CitiesActivity;
import ir.alibabae.R;

public class InsideFlightFragment extends Fragment {
    View view;
    Button button;
    TextView txtSource,txtDestination,txtDate;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.inside_fight_fragment,container,false);
        setUpViews();

        button.setOnClickListener(v -> {

        });
        txtSource.setOnClickListener(v -> {
            Intent intent= new Intent(getContext(), CitiesActivity.class);
            getActivity().startActivityForResult(intent,77);

        });

        txtDestination.setOnClickListener(v -> {

        });

        txtDate.setOnClickListener(v -> {

        });

        return  view;
    }

    private void setUpViews() {
        button = view.findViewById(R.id.btn_iff_fine);
        txtSource = view.findViewById(R.id.txt_iff_source);
        txtDestination = view.findViewById(R.id.txt_itt_destination);
        txtDate = view.findViewById(R.id.txt_itt_date);
    }
}
